﻿using System;
using System.IO;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Threading;


namespace Search
{
    public partial class MainForm : Form
    {
        private ManualResetEvent _pauseEvent = new ManualResetEvent(true);
        private int FilesFound = 0;
        private string current_node;            // keeps last using folder node in form of system path
        private string error_log_file = "errors.txt";

        private void SearchDirs(string dir, string pattern)
        {
            try
            {
                foreach (string d in Directory.EnumerateDirectories(dir))
                {
                    if (backgroundWorker.CancellationPending == true)
                        return;

                    _pauseEvent.WaitOne();

                    SearchDirs(d, pattern);
                    SearchFiles(d, pattern);
                }
            }
            catch (UnauthorizedAccessException ex) { }
            catch (IOException ex) { }
        }

        private void SearchFiles(string dir, string pattern)
        {
            var files = new List<string>();

            try
            {
                foreach (string file in Directory.EnumerateFiles(dir, pattern))
                {
                    if (backgroundWorker.CancellationPending == true)
                        return;

                    _pauseEvent.WaitOne();

                    if (search_substring == String.Empty)       // just search files by pattern
                        files.Add(file);
                    else if (SearchInFile(file))                 // scan files too
                        files.Add(file);
                }
            }
            catch (UnauthorizedAccessException ex) { }
            catch (IOException ex) { }

            // report progress only when we're done with the dir and have any files found
            if (files.Count > 0)
            {
                FilesFound += files.Count;
                backgroundWorker.ReportProgress(FilesFound, Tuple.Create(dir, files));
            }
        }

        private bool SearchInFile(string file)
        {
            foreach (string line in File.ReadLines(file))
            {
                if (line.Contains(search_substring))
                    return true;
            }

            return false;
        }

        private void SetWorkerMode(bool running)
        {
            if(running)
            {
                _pauseEvent.Set();
            }
            else
            {
                _pauseEvent.Reset();
            }
        }

        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var args = e.Argument as Tuple<string, string>;
            foreach (string d in Directory.EnumerateDirectories(args.Item1))
            {
                if (backgroundWorker.CancellationPending == true)
                {
                    e.Cancel = true;
                    return;
                }

                SearchDirs(d, args.Item2);
                SearchFiles(d, args.Item2);
            }
        }

        private void BackgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                var file_path = e.UserState as Tuple<string, List<string>>;
                DirectoryInfo di = new DirectoryInfo(file_path.Item1);

                // the next path where files were found
                string next_path = di.FullName + Path.DirectorySeparatorChar;

                // find intersection between next and current folders
                // for example, between c:\Program Files\Git\bin\ and c:\Program Files\Git\etc\pki\ca-trust\extracted\ it will be c:\Program Files\Git\
                string common_path = (current_node == search_root_path) ? search_root_path : FindCommonPath(current_node, next_path);

                // find this node
                TreeNode selected_node = treeFiles.Nodes.Find(common_path, true).FirstOrDefault();

                if (next_path.Length > common_path.Length)
                {
                    // find and create missing nodes
                    // following previous example, it will be etc\pki\ca-trust\extracted\
                    selected_node = CreateDirNodes(selected_node, next_path.Replace(common_path, ""));
                    current_node = selected_node.Name;
                }

                // create found files nodes
                CreateFileNodes(selected_node, file_path.Item2);
                labelProcessedFiles.Text = (FilesFound).ToString();
            }
            catch(Exception ex)
            {
                StreamWriter error_file = File.AppendText(error_log_file);
                error_file.WriteLine(ex.ToString());
                error_file.Close();
                error_file.Dispose();
            }
        }

        private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            timer.Stop();
            buttonStart.Text = "Start";
            buttonPause.Enabled = false;
            SetWorkerMode(false);

            if (e.Error != null)
                throw new Exception("BackGroundWorker Error: ", e.Error);
            else if (e.Cancelled == true)
                SetStatus("Canceled");
            else
                SetStatus("Done");
        }
    }
}
