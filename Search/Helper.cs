﻿// file contains different help methods

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Search
{
    public partial class MainForm : Form
    {
        private void ClearItems()
        {
            FilesFound = 0;
            labelTimeSpent.Text = "00:00";
            labelProcessedFiles.Text = "0";
            labelCurrentFile.Text = "";
            treeFiles.Nodes.Clear();
        }

        private void SetStatus(string msg)
        {
            labelStatus.Text = msg;
        }

        private void SaveQuery(ComboBox combo)
        {
            if (combo.Text != string.Empty && !combo.Items.Contains(combo.Text))
                combo.Items.Add(combo.Text);
        }

        private string FindCommonPath(string current_path, string new_path)
        {
            int min = Math.Min(current_path.Length, new_path.Length);

            int idx = 0;
            while (idx < min && current_path[idx] == new_path[idx])
                ++idx;

            string real_new_path = new_path.Substring(0, idx);
            
            // erase all after last separator char
            real_new_path = real_new_path.Substring(0, real_new_path.LastIndexOf(Path.DirectorySeparatorChar) + 1);

            return real_new_path;
        }
    }
}
