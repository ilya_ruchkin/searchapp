﻿namespace Search
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.comboPath = new System.Windows.Forms.ComboBox();
            this.comboContains = new System.Windows.Forms.ComboBox();
            this.comboPattern = new System.Windows.Forms.ComboBox();
            this.labelPath = new System.Windows.Forms.Label();
            this.labelPattern = new System.Windows.Forms.Label();
            this.labelContains = new System.Windows.Forms.Label();
            this.labelCurrentFile = new System.Windows.Forms.Label();
            this.labelProcessedFiles = new System.Windows.Forms.Label();
            this.labelTimeSpent = new System.Windows.Forms.Label();
            this.buttonPause = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.treeFiles = new System.Windows.Forms.TreeView();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // comboPath
            // 
            this.comboPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboPath.Font = new System.Drawing.Font("Carlito", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboPath.FormattingEnabled = true;
            this.comboPath.Location = new System.Drawing.Point(121, 47);
            this.comboPath.Margin = new System.Windows.Forms.Padding(4);
            this.comboPath.Name = "comboPath";
            this.comboPath.Size = new System.Drawing.Size(436, 27);
            this.comboPath.TabIndex = 0;
            this.comboPath.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_KeyDown);
            // 
            // comboContains
            // 
            this.comboContains.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboContains.Font = new System.Drawing.Font("Carlito", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboContains.ForeColor = System.Drawing.SystemColors.WindowText;
            this.comboContains.FormattingEnabled = true;
            this.comboContains.Location = new System.Drawing.Point(121, 117);
            this.comboContains.Margin = new System.Windows.Forms.Padding(4);
            this.comboContains.Name = "comboContains";
            this.comboContains.Size = new System.Drawing.Size(436, 27);
            this.comboContains.TabIndex = 2;
            this.comboContains.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_KeyDown);
            // 
            // comboPattern
            // 
            this.comboPattern.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboPattern.Font = new System.Drawing.Font("Carlito", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboPattern.ForeColor = System.Drawing.SystemColors.WindowText;
            this.comboPattern.FormattingEnabled = true;
            this.comboPattern.Location = new System.Drawing.Point(121, 82);
            this.comboPattern.Margin = new System.Windows.Forms.Padding(4);
            this.comboPattern.Name = "comboPattern";
            this.comboPattern.Size = new System.Drawing.Size(436, 27);
            this.comboPattern.TabIndex = 1;
            this.comboPattern.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Combo_KeyDown);
            // 
            // labelPath
            // 
            this.labelPath.AutoSize = true;
            this.labelPath.Location = new System.Drawing.Point(16, 47);
            this.labelPath.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPath.Name = "labelPath";
            this.labelPath.Size = new System.Drawing.Size(90, 19);
            this.labelPath.TabIndex = 3;
            this.labelPath.Text = "Search path:";
            // 
            // labelPattern
            // 
            this.labelPattern.AutoSize = true;
            this.labelPattern.Location = new System.Drawing.Point(16, 82);
            this.labelPattern.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPattern.Name = "labelPattern";
            this.labelPattern.Size = new System.Drawing.Size(88, 19);
            this.labelPattern.TabIndex = 4;
            this.labelPattern.Text = "File pattern:";
            // 
            // labelContains
            // 
            this.labelContains.AutoSize = true;
            this.labelContains.Location = new System.Drawing.Point(16, 117);
            this.labelContains.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelContains.Name = "labelContains";
            this.labelContains.Size = new System.Drawing.Size(94, 19);
            this.labelContains.TabIndex = 5;
            this.labelContains.Text = "File contains:";
            // 
            // labelCurrentFile
            // 
            this.labelCurrentFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCurrentFile.Location = new System.Drawing.Point(121, 352);
            this.labelCurrentFile.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelCurrentFile.Name = "labelCurrentFile";
            this.labelCurrentFile.Size = new System.Drawing.Size(365, 19);
            this.labelCurrentFile.TabIndex = 7;
            this.labelCurrentFile.Text = "...";
            // 
            // labelProcessedFiles
            // 
            this.labelProcessedFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProcessedFiles.Location = new System.Drawing.Point(494, 352);
            this.labelProcessedFiles.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelProcessedFiles.Name = "labelProcessedFiles";
            this.labelProcessedFiles.Size = new System.Drawing.Size(63, 19);
            this.labelProcessedFiles.TabIndex = 8;
            this.labelProcessedFiles.Text = "0";
            this.labelProcessedFiles.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelTimeSpent
            // 
            this.labelTimeSpent.Location = new System.Drawing.Point(20, 151);
            this.labelTimeSpent.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelTimeSpent.Name = "labelTimeSpent";
            this.labelTimeSpent.Size = new System.Drawing.Size(84, 19);
            this.labelTimeSpent.TabIndex = 9;
            this.labelTimeSpent.Text = "00:00";
            this.labelTimeSpent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonPause
            // 
            this.buttonPause.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPause.BackColor = System.Drawing.Color.White;
            this.buttonPause.Enabled = false;
            this.buttonPause.FlatAppearance.BorderSize = 0;
            this.buttonPause.Font = new System.Drawing.Font("Carlito", 12F);
            this.buttonPause.Location = new System.Drawing.Point(594, 80);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(78, 27);
            this.buttonPause.TabIndex = 4;
            this.buttonPause.Tag = "";
            this.buttonPause.Text = "Pause";
            this.buttonPause.UseVisualStyleBackColor = false;
            this.buttonPause.Click += new System.EventHandler(this.ButtonPause_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStart.BackColor = System.Drawing.Color.White;
            this.buttonStart.FlatAppearance.BorderSize = 0;
            this.buttonStart.Font = new System.Drawing.Font("Carlito", 12F);
            this.buttonStart.Location = new System.Drawing.Point(594, 47);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(78, 27);
            this.buttonStart.TabIndex = 3;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = false;
            this.buttonStart.Click += new System.EventHandler(this.ButtonStart_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelStatus.Location = new System.Drawing.Point(151, 9);
            this.labelStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(350, 19);
            this.labelStatus.TabIndex = 10;
            this.labelStatus.Text = "Enter the values, please.";
            this.labelStatus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // treeFiles
            // 
            this.treeFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeFiles.Location = new System.Drawing.Point(121, 151);
            this.treeFiles.Name = "treeFiles";
            this.treeFiles.Size = new System.Drawing.Size(436, 198);
            this.treeFiles.TabIndex = 11;
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BackgroundWorker_ProgressChanged);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BackgroundWorker_RunWorkerCompleted);
            // 
            // MainForm
            // 
            this.AcceptButton = this.buttonStart;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(684, 406);
            this.Controls.Add(this.treeFiles);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.buttonPause);
            this.Controls.Add(this.labelTimeSpent);
            this.Controls.Add(this.labelProcessedFiles);
            this.Controls.Add(this.labelCurrentFile);
            this.Controls.Add(this.labelContains);
            this.Controls.Add(this.labelPattern);
            this.Controls.Add(this.labelPath);
            this.Controls.Add(this.comboPattern);
            this.Controls.Add(this.comboContains);
            this.Controls.Add(this.comboPath);
            this.Font = new System.Drawing.Font("Carlito", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(700, 445);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Search";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboPath;
        private System.Windows.Forms.ComboBox comboContains;
        private System.Windows.Forms.ComboBox comboPattern;
        private System.Windows.Forms.Label labelPath;
        private System.Windows.Forms.Label labelPattern;
        private System.Windows.Forms.Label labelContains;
        private System.Windows.Forms.Label labelCurrentFile;
        private System.Windows.Forms.Label labelProcessedFiles;
        private System.Windows.Forms.Label labelTimeSpent;
        private System.Windows.Forms.Button buttonPause;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.TreeView treeFiles;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
    }
}

