﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Search
{
    public partial class MainForm : Form
    {

        private TreeNode CreateRoot(string dir)
        {
            DirectoryInfo di = new DirectoryInfo(dir);
            TreeNode root = treeFiles.Nodes.Add(di.FullName);
            root.Name = di.FullName;
            current_node = root.Name;
            return root;
        }

        private TreeNode CreateDirNodes(TreeNode parent, string nodes)
        {
            if (parent == null)
                throw new ArgumentNullException();

            TreeNode next = parent;
            string[] directories = nodes.Remove(nodes.Length - 1).Split(Path.DirectorySeparatorChar);

            for (int i = 0; i < directories.Length; ++i)
            {
                next = next.Nodes.Add(directories[i]);
                next.Name = next.Parent.Name + directories[i] + Path.DirectorySeparatorChar;
            }

            return next;
        }

        private void CreateFileNodes(TreeNode parent, List<string> files)
        {
            if (parent == null)
                throw new ArgumentNullException();

            foreach(string file in files)
            {
                FileInfo fi = new FileInfo(file);
                parent.Nodes.Add(fi.Name);
                labelCurrentFile.Text = fi.FullName;
            }
        }
    }
}
