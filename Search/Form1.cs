﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Timers;
using System.Threading;


namespace Search
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private DateTime start_time;
        private System.Timers.Timer timer = new System.Timers.Timer(1000);
        private string search_root_path;
        private string search_file_pattern;
        private string search_substring;

        // event handlers
        private void ButtonStart_Click(object sender, EventArgs e)
        {
            if (buttonStart.Text == "Stop")
            {
                backgroundWorker.CancelAsync();
                return;
            }

            search_root_path = comboPath.Text;
            search_file_pattern = comboPattern.Text;
            search_substring = comboContains.Text;

            if (!Directory.Exists(search_root_path))
            {
                SetStatus("Path not found");
                return;
            }

            buttonStart.Text = "Stop";
            buttonPause.Enabled = true;
            SetStatus("Searching...");

            // save currently searched queries
            SaveQuery(comboPath);
            SaveQuery(comboPattern);
            SaveQuery(comboContains);

            // activate timer
            start_time = DateTime.Now;
            timer.Elapsed -= HandleTimerElapsed;
            timer.Elapsed += HandleTimerElapsed;
            timer.Start();

            // clear info labels
            ClearItems();

            // create root node at tree
            treeFiles.SelectedNode = CreateRoot(search_root_path);
            
            if (!backgroundWorker.IsBusy)
            {
                SetWorkerMode(true);
                backgroundWorker.RunWorkerAsync(Tuple.Create(search_root_path, search_file_pattern));
            }
        }

        private void ButtonPause_Click(object sender, EventArgs e)
        {
            if (buttonPause.Text == "Pause")
            {
                SetWorkerMode(false);
                timer.Stop();
                buttonPause.Text = "Continue";
                SetStatus("Paused");
            }
            else
            {
                SetWorkerMode(true);
                timer.Start();
                buttonPause.Text = "Pause";
                SetStatus("Searching...");
            }
        }

        public void HandleTimerElapsed(object sender, ElapsedEventArgs e)
        {
            this.labelTimeSpent.Invoke((MethodInvoker)delegate
           {
               TimeSpan diff = DateTime.Now - start_time;
               labelTimeSpent.Text = string.Format("{0:D2}:{1:D2}", diff.Minutes, diff.Seconds);
           });
        }

        private void Combo_KeyDown(object sender, KeyEventArgs e)
        {
            ComboBox combo = sender as ComboBox;
            if (e.KeyCode == Keys.Down)
                combo.DroppedDown = true;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try { SaveItems(); }
            catch(Exception ex) { SetStatus(ex.ToString()); }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try { ReadItems(); }
            catch (Exception ex) { SetStatus(ex.ToString()); }
        }
    }
}
