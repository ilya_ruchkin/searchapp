﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Search
{
    public partial class MainForm : Form
    {
        private string file_path = "history.txt";
        private string path_mark = "[Paths]";
        private string pattern_mark = "[Patterns]";
        private string contains_mark = "[Contains]";

        private void SaveItems()
        {
            if (File.Exists(file_path))
                File.WriteAllText(file_path, string.Empty);

            StreamWriter save_file = new StreamWriter(file_path);

            save_file.WriteLine(path_mark);
            foreach (string item in comboPath.Items)
                save_file.WriteLine(item);

            save_file.WriteLine(pattern_mark);
            foreach (string item in comboPattern.Items)
                save_file.WriteLine(item);

            save_file.WriteLine(contains_mark);
            foreach (string item in comboContains.Items)
                save_file.WriteLine(item);

            save_file.Close();
            save_file.Dispose();
        }

        private void ReadItems()
        {
            if (!File.Exists(file_path))
                return;

            string[] lines = File.ReadAllLines(file_path);

            if (!lines.Contains(path_mark) || !lines.Contains(path_mark) || !lines.Contains(path_mark))
                throw new Exception("File corrupted");

            if (lines.Length == 3)
                return;              // file is empty

            int i = 1;
            for(; lines[i] != pattern_mark; ++i)
                comboPath.Items.Add(lines[i]);

            for(++i; lines[i] != contains_mark; ++i)
                comboPattern.Items.Add(lines[i]);

            for (++i; i < lines.Length; ++i)
                comboContains.Items.Add(lines[i]);
        }
    }
}
